import React from 'react';
import { StyleSheet, Text, View, Button, FlatList } from 'react-native';
import axios from "axios";
import MovieCard from './src/shared/movie-card/movie-card';

export default class Page2 extends React.Component {
    static navigationOptions = {
        title: 'Page 2'
    };

    render() {
        return (
            <View style={styles.container}>
                <Text>ID: {this.props.navigation.getParam("id", 0)}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
