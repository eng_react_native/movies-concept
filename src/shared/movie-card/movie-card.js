import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import moment from "moment";

export default class MovieCard extends React.Component {
    constructor(props){
        super(props);
        console.log(props);
    }
  render() {
    return (
      <View style={styles.container}>
        <Image
            source={{
                uri: "https://image.tmdb.org/t/p/w500" + this.props.movie.backdrop_path 
            }}
            style={{
                width: "100%",
                height: 200
            }}
         />
         <Text>
            {this.props.movie.title}
        </Text>
        <Text>
            {this.props.movie.overview}
        </Text>
        <Text>
            {moment(this.props.movie.release_date).format("DD/MM/YYYY HH:mm:ss")}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
