import React from 'react';
import { StyleSheet, Text, View, Button, FlatList } from 'react-native';
import axios from "axios";
import MovieCard from './src/shared/movie-card/movie-card';

export default class App extends React.Component {
    static navigationOptions = {
        title: 'Home',
        headerRight: (
            <Button
              onPress={() => alert('This is a button!')}
              title="Add"
              color="blue"
            />
          ),
    };

    state = {
        movies: [],
        loading: true
    }

    componentDidMount() {
        var primeiroArray = [
            "asdfasd",
            "asdfasfasd",
            "asdfasdfasasdfasdfaasdfasd"
        ]

        var segundoArray = [
            "asdfasd",
        ]

        var terceiroArray =
            primeiroArray.concat(segundoArray);

        var url = "https://api.themoviedb.org/3/movie/popular?api_key=51e4e9d52532d389174b5252cd99d33d&page=" + this.state.page;
        axios.get(url).then(({ data }) => {
            var actualMovies = this.state.movies;
            var movesConcatenated =
                actualMovies.concat(data.results)

            this.setState({
                movies: data.results,
                loading: false
            });
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Button
                    title={"Go to Page 2"}
                    onPress={() => {
                        this.props.navigation.navigate("Page2", {
                            id: 10
                        });
                    }}
                    />
                {this.state.loading
                    ? <Text>Loading...</Text>
                    : <FlatList
                        data={this.state.movies}
                        renderItem={({ item }) => <MovieCard movie={item} />}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={() => {
                            // Incrementar variavel de página
                            // 
                            alert("end reached");
                        }}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
